import { screen, render } from '@testing-library/react'

import SiteHeader from '.'

describe('<Header />', () => {
  it('should render colors correctly', () => {
    const { container } = render(<SiteHeader />)
    expect(container.firstChild).toHaveStyle({
      'background-color': '#ffff'
    })
  })
})
