import * as S from './styles'
import React from 'react'
import { useCart } from "../../../context/cartContext";

const CreditCard = ({
    creditDefaultCard = '* * * *   * * * *   * * * *   * * * *',
    nameDefaultHolder = 'Nome do titular',
    nameDefaultValidity = '00/00'
  }) => {
    const { cart } = useCart();
    const  creditCard =  cart?.creditCard?.creditNumber? cart.creditCard.creditNumber:creditDefaultCard
    const nameHolder =  cart?.creditCard?.name? cart.creditCard.name:nameDefaultHolder
    const nameValidity =  cart?.creditCard?.vadidate? cart.creditCard.vadidate:nameDefaultValidity
    const cvv =  cart?.creditCard?.CVV? cart.creditCard.CVV:'***'
    let bgcard= ' '
    let brand = ' '
    
    if(creditCard!=creditDefaultCard){
        bgcard= 'filled' 
        if(nameValidity!=nameDefaultValidity && nameValidity.length == 5 ){
            bgcard = 'verse'
        }
    }

    
    if(creditCard){
        brand = creditCard.slice(0, 4)
        if (brand.substring(0, 1) == 4) {
            brand="visa"
        }
        if (brand.substring(0, 1) == 5) {
            brand="master"
        }
    }

    return(
        <S.creditCard className={bgcard}>
            <S.brand className={brand}/>

            <S.cardNumber>
                {creditCard}
            </S.cardNumber>
            <S.cardDetails>
            <span>
                {nameHolder}
            </span>
            <span>
                {nameValidity}
            </span>
            <span className='cvv'>
                {cvv}
            </span>
            </S.cardDetails>
        </S.creditCard>
    )
}
export default CreditCard