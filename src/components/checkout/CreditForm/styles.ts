import styled from 'styled-components'

export const ContentInput = styled.fieldset`
    width:100%;
    border:none;
    display:flex;
    margin: 0 5px;
    &.algnRIght{
        justify-content: flex-end;
    }
`