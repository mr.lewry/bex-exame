import React from "react"
import * as S from './styles'

const Button = ({ label , type }:{ label :any, type:any }) => (
    <S.Button className="material-input" type={type}>
      {label}
    </S.Button>
)

export default Button
