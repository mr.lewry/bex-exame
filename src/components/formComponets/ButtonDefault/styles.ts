import styled from 'styled-components'

export const Button = styled.button.attrs({
        type:"submit"
    })`
    background: ${props => props.theme.colors.primary} 0% 0% no-repeat padding-box;
    border-radius: 10px;
    opacity: 1;
    width: 100%;
    height: 45px;
    border: none;
    position:relative;
    margin-top:62px;
    font: normal normal 600 12px sans-serif;
    letter-spacing: -0.01px;
    letter-spacing: 2px;
    color:  ${props => props.theme.colors.textLight};
    text-transform:uppercase;
    opacity: 1;
    @media(min-width:${props => props.theme.breakpoints.medium}){
        width: 246px;
    }
`
