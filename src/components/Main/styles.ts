import styled from 'styled-components'


export const Wrapper = styled.main`
  width: 100%;
  min-height: 100%;
  background-color: ${props => props.theme.colors.background};
  color: ${props => props.theme.colors.textLight};
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  @media(min-width:${props => props.theme.breakpoints.medium}){
    justify-content: start;
  }
`
export const container =  styled.section`
  padding-top:65px;
  background: ${props => props.theme.colors.background};
  justify-content:center;
  align-items:center;
  display:flex;
  flex-direction:column;
  flex-wrap:nowrap;
  .default{
    width:100%;
    min-height:285px;
    background:#f3f3f3;
  }
  .default{
    min-height:285px;
    background:#f3f3f3;
  }
  
  @media(min-width:${props => props.theme.breakpoints.medium}){
    display:grid;
    grid-column-gap: 15px;
    grid-template-columns: 4fr 1fr;
    align-items: start;
  }
`

