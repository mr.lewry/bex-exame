import * as S from './styles'

import SiteHeader from '../Header/index'
import Checkout from '../Checkout/index'
import CartProvider from '../../context/cartContext'

const Main = () => (
  <CartProvider>
    <S.Wrapper>
      <SiteHeader/>
      <S.container className="container">
        <Checkout/>
        
        <div className='default'>
          teste
        </div>

      </S.container>
    </S.Wrapper>
  </CartProvider>
)

export default Main
