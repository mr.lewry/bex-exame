import React from "react";

export interface cartInterface{
  creditNumber: Number
  name: String
  vadidate: String 
  CVV: Number
}

const CartContext = React.createContext({});

export default function CartProvider({ children }:{ children:any }) {
  const [cart, setCart] = React.useState<cartInterface | undefined>(undefined);

  return (
    <CartContext.Provider
      value={{
        cart,
        setCart
      }}
    >
      {children}
    </CartContext.Provider>
  );
}

export function useCart() {
  const context = React.useContext(CartContext);
  if (!context) throw new Error("useCart must be used within CartProvider");
  const { cart, setCart } = context;
  return context;
}


