import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`
    @font-face {
        font-family: 'Verdana';
        src: local('Verdana'), local('VerdadaRegular'),
        url(./Verdana/VerdanaRegular.ttf) format('ttf');
        font-weight: 300;
        font-style: normal;
    }

    @font-face {
        font-family: 'Verdana';
        src: local('Verdana'), local('VerdanaBold'),
        url(./Verdana/VerdanaBold.ttf) format('ttf');
        font-weight: 800;
        font-style: bold;
    }

    @font-face {
        font-family: 'SF';
        src: local('SansSerif'), local('VerdanaRegular'),
        url(./SansSerifPro/SfProRegular.otf) format('otf');
        font-weight: 300;
        font-style: Normal;
    }

    @font-face {
        font-family: 'SF';
        src: local('SansSerif'), local('VerdanaRegular'),
        url(./SansSerifPro/SfProSemibold.otf) format('otf');
        font-weight: 600;
    }



`
