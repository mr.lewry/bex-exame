const theme = {
  colors: {
    background: '#F7F7F7',
    text: '#C9C9C9',
    textDark: '#3C3C3C',
    textLight: '#FFFFFF',
    primary: '#DE4B4B',
    whiteBg: '#FFFFFF',
    shadown: '#000000B3'
  },
  breakpoints: {
    small: '424px',
    medium: '768px',
    large: '992px',
    xLarge: '1200px',
    xxLarge: '1400px'
  }
}

export default theme
